#ifndef CTRL_UNIT_HPP
#define CTRL_UNIT_HPP

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(CTRL_UNIT) 
{
  sc_in<sc_bv<6> > Operation;
  sc_out<bool> RegDst, ALUSrc, MemtoReg, Jump, RegWrite, MemRead, MemWrite, Branch;
  sc_out<sc_bv<2> > ALU_Op;	
  
  SC_CTOR(CTRL_UNIT) 
  {
    SC_THREAD(behav);
      sensitive << Operation;
  } 
  
 private:
  void behav();
};

#endif
