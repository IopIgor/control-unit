#include <systemc.h>
#include "CTRL_UNIT.hpp"

using namespace std;
using namespace sc_core;

void CTRL_UNIT::behav()
{
   while(true)
   {
   	 wait();
     switch (Operation->read().to_uint()) //because switch wants an integer argument
     {
        case 0: 			//add: 000000
			RegDst->write(1);
			ALUSrc->write(0);
			MemtoReg->write(0);
			Jump->write(0);
			RegWrite->write(1);
			MemRead->write(0);
			MemWrite->write(0);
			Branch->write(0);
			ALU_Op->write("10");
		break;

		case 8:		 		//addi: 001000
			RegDst->write(0);
			ALUSrc->write(1);
			MemtoReg->write(0);
			Jump->write(0);
			RegWrite->write(1);
			MemRead->write(0);
			MemWrite->write(0);
			Branch->write(0);
			ALU_Op->write("00");
		break;

		case 43:	 		//sw: 101011
			RegDst->write(0);
			ALUSrc->write(1);
			MemtoReg->write(0);
			Jump->write(0);
			RegWrite->write(0);
			MemRead->write(0);
			MemWrite->write(1);
			Branch->write(0);
			ALU_Op->write("00");
		break;

		case 35: 			//ld: 100011 
			RegDst->write(0);
			ALUSrc->write(1);
			MemtoReg->write(1);
			Jump->write(0);
			RegWrite->write(1);
			MemRead->write(1);
			MemWrite->write(0);
			Branch->write(0);
			ALU_Op->write("00");
		break;

		case 4:		 		//bne: 000100
			RegDst->write(0);
			ALUSrc->write(0);
			MemtoReg->write(0);
			Jump->write(0);
			RegWrite->write(0);
			MemRead->write(0);
			MemWrite->write(0);
			Branch->write(1);
			ALU_Op->write("01");
		break;

		case 2:		 		 //j: 000010
			RegDst->write(0);
			ALUSrc->write(0);
			MemtoReg->write(0);
			Jump->write(1);
			RegWrite->write(0);
			MemRead->write(0);
			MemWrite->write(0);
			Branch->write(0);
			ALU_Op->write("01");
		break;
     }
   }
}