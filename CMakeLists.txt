cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (CTRL_UNIT SHARED "src/CTRL_UNIT.cpp")
add_executable (test_CTRL_UNIT test/test_CTRL_UNIT.cpp)
target_link_libraries (test_CTRL_UNIT CTRL_UNIT systemc)

enable_testing ()

add_test (CTRL_UNIT test_CTRL_UNIT)
