#include <systemc.h>
#include <iostream>
#include "CTRL_UNIT.hpp"

using namespace std;
using namespace sc_core;

const sc_time wait_time(5, SC_NS);

SC_MODULE(TestBench) 
{
 public:
  sc_signal<sc_bv<6> > Operation;
  sc_signal<bool> RegDst, ALUSrc, MemtoReg, Jump, RegWrite, MemRead, MemWrite, Branch;
  sc_signal<sc_bv<2> > ALU_Op;	

  CTRL_UNIT ctrl;

  SC_CTOR(TestBench) : ctrl("ctrl")
  {
    SC_THREAD(stimulus_thread);
    SC_THREAD(watcher_thread);
      sensitive << Operation;
    SC_THREAD(check);
      sensitive << Operation;

    ctrl.Operation(this->Operation);

    ctrl.RegDst(this->RegDst);
    ctrl.ALUSrc(this->ALUSrc);
    ctrl.MemtoReg(this->MemtoReg);
    ctrl.Jump(this->Jump);
    ctrl.RegWrite(this->RegWrite);
    ctrl.MemRead(this->MemRead);
    ctrl.MemWrite(this->MemWrite);
    ctrl.Branch(this->Branch);
    ctrl.ALU_Op(this->ALU_Op);
  }

  bool Check() const {return error;}
  
 private:
  bool error;

  void stimulus_thread() 
  { //tested 2 significative operations
    Operation.write("100011");
    wait(wait_time);

    Operation.write("000000");
    wait(wait_time);
  }

  void watcher_thread()
  {
    while(true)
    {
      wait();
      wait(1, SC_PS); //to avoid the execution of "if" condition before execution of the component
      cout << "At time " << sc_time_stamp() << ":\nFor the input " << Operation.read() 
           << " the outputs of the control unit are:" << "\nRegDst: " << RegDst.read() 
           << "\nALUSrc: " << ALUSrc.read() << "\nMemtoReg: " << MemtoReg.read() 
           << "\nJump: " << Jump.read() << "\nRegWrite: " << RegWrite.read() 
           << "\nMemRead: " << MemRead.read() << "\nMemWrite: " << MemWrite.read() 
           << "\nBranch: " << Branch.read() << "\nALU_Op: " << ALU_Op.read() << endl;
    }
  }

  void check() 
  {
    error = 0;
    
    wait();
    wait(1, SC_PS); //to avoid the execution of "if" condition before execution of the component
    if(RegDst.read() != 0 || ALUSrc.read() != 1 || MemtoReg.read() != 1 || Jump.read() != 0 || 
       RegWrite.read() != 1 || MemRead.read() != 1 || MemWrite.read() != 0 || 
       Branch.read() != 0 || ALU_Op.read() != "00")
    {
      cout << "test failed!!\nThe couples of theoretical values - test's results are:" << "\n0-" 
           << RegDst.read() << "\n1-" << ALUSrc.read() << "\n1-" << MemtoReg.read() << "\n0-" 
           << Jump.read() << "\n1-" << RegWrite.read() << "\n1-" << MemRead.read() << "\n0-" 
           << MemWrite.read() << "\n0-" << Branch.read() << "\n00-" << ALU_Op.read() << endl << endl;
      error = 1;
    }

    wait();
    wait(1, SC_PS); //to avoid the execution of "if" condition before execution of the component
    if(RegDst.read() != 1 || ALUSrc.read() != 0 || MemtoReg.read() != 0 || Jump.read() != 0 || 
       RegWrite.read() != 1 || MemRead.read() != 0 || MemWrite.read() != 0 || 
       Branch.read() != 0 ||ALU_Op.read() != "10")
    {
      cout << "test failed!!\nThe couples of theoretical values - test's results are:" << "\n1-" 
           << RegDst.read() << "\n0-" << ALUSrc.read() << "\n0-" << MemtoReg.read() << "\n0-" 
           << Jump.read() << "\n1-" << RegWrite.read() << "\n0-" << MemRead.read() << "\n0-" 
           << MemWrite.read() << "\n0-" << Branch.read() << "\n10-" << ALU_Op.read() << endl << endl;
      error = 1;
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.Check();
}


